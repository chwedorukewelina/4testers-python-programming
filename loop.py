def temperature_pressure(celcjus, hektopashal):
    if celcjus == 0 and hektopashal == 1013:
        return True
    else:
        return False


def mark(point):
    if point >= 90:
        return 5
    elif point >= 75:
        return 4
    elif point >= 50:
        return 3
    else:
        return 2


def numbers_devision_for_seven(from_number, to_number):
    for i in range(from_number, to_number + 1):
        if i % 7 == 0:
            print(f"number {i} is devisible number 7")


def convert_celcjusz_to_fahenreit(celcjusz):
    return 9 / 5 * celcjusz + 32


def print_temperature_in_celcjus_and_fahenreit(temperature_in_celcius_list):
    for temp in temperature_in_celcius_list:
        convert_temp = round(convert_celcjusz_to_fahenreit(temp), 2)
        print(f"temperature in celcius {temp} temperature in fahenrait:{convert_temp}")


if __name__ == '__main__':
    normal = temperature_pressure(0, 1013)
    print(normal)
    print(f":{temperature_pressure(0, 1013)}")
    print(f":{temperature_pressure(0, 1014)}")

print(f":{mark(30)}")
print(f"f{numbers_devision_for_seven(0, 30)}")

spring_temperatures = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
print_temperature_in_celcjus_and_fahenreit(spring_temperatures)
