from faker import Faker
import random

fake = Faker()


def describe_of_person():
    return {
        "email": fake.ascii_safe_email(),
        "seniority_years": random.randint(0, 40),
        "female": random.choice([True, False])
    }


def generate_array_of_employee_dictonaries(number_of_dictionaries):
    return [describe_of_person() for _ in range(number_of_dictionaries)]


def get_emails_of_employess_with_seniority_years_greater_then_10(list_of_employess):
    output_emails = []

    for employee_dictionary in list_of_employess:
        if employee_dictionary["seniority_years"] > 10:
            output_emails.append(employee_dictionary["email"])

    return output_emails


if __name__ == '__main__':
    for n in range(100):
        print(describe_of_person())

generated_employess = generate_array_of_employee_dictonaries(20)
print(generated_employess)

filtred_emails= get_emails_of_employess_with_seniority_years_greater_then_10(generated_employess)
print(filtred_emails)
