def describe_to_player(player_data):
    print(f"The player {player_data['nick']} is of type {player_data['type']} and has {player_data['exp_points']} EXP")


def find_divisible_subset(number_from, number_to, divisible_by):
    for number in range(number_from, number_to + 1):
        if number % divisible_by == 0:
            print(number)


def celcius_to_fahenreit(lists_of_celcius_temperature):
    fahenreit = []
    for i in lists_of_celcius_temperature:
        t_fahenreit = i* 9 / 5 + 32
        fahenreit.append(t_fahenreit)
        return fahenreit


if __name__ == '__main__':

    player = {
    "nick": 'maestro_54',
    "type": 'warrior',
    "exp_points": '3000'
}

describe_to_player(player)

find_divisible_subset(1, 50, 9)

print(celcius_to_fahenreit([10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]))
