import random
import string


def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)
    return result_str


def data_grnerate(mail):
    return {
        "email": mail,
        "password": get_random_string(12),
    }


if __name__ == '__main__':
    friend = {
        "name": "Anna",
        "age": 34,
        "hobby": ["bartender", "majorka"],
    }

friend["name"] = "bartosz"
friend["age"] = friend["age"] + 1
friend["age"] += 2
friend["hobby"].append("boys")
print(friend)


dane = data_grnerate("ewelina@gamil.com")
print(dane)
