def hello(name, city):
        greeting= f"Witaj{name.upper()}!Milo cie widziec w naszym miescie:{city.upper()}!"
        print(greeting)


if __name__ == '__main__':
    first_name = "Ewelina"
    last_name = "Chwedoruk"
    email = first_name.lower() + "." + last_name.lower() + "@4testers.pl"
    print(email)

    email_formatted = f"{first_name.lower()}.{last_name.lower()}@4testers.pl"
    print(email_formatted)

    hello("Beata", "Torun")
    hello("michal", "gdynia")