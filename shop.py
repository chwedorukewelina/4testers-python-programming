class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):

        self.products.append(product)

    def get_total_price(self):

        prices = []

        for i in self.products:
            prices.append(i.get_price())

        return sum(prices)

    def get_total_quantity_of_products(self):

        quantities = 0


        for i in self.products:
            quantities += i.quantity

        return quantities


    def purchase(self):

        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        prices = 0

        prices = self.unit_price * self.quantity


        return prices


if __name__ == '__main__':
    Order1 = Order('adrian@example.com')
    test_product = Product('Shoes', 100.0)
    Product2 = Product('Shoes', 30.00, 3.0)
    tshirt = Product('T-Shirt', 50.00, 2.0)
    bag = Product('Bag', 10.00)
