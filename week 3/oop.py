class Car:
    def __init__(self, brand, colour, year):
        self.brand= brand
        self.colour = colour
        self.year = year
        self.milleage = 0


if __name__ == '__main__':

    car1 = Car("mercedes", "blue", "2020" )
    car2 = Car("maluch", "red", "1984")
    car3 = Car("polonez", "white", "2000" )

    print(car1)
    print(car2)
    print(car3)