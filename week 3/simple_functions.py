def is_adult (age):
    if age >= 18:
        return True
    else:
        False

def return_word_containing_letter_a(word_list):
    return[word for word in word_list if 'a' in word]