from simple_functions import is_adult, return_word_containing_letter_a


def test_is_adult_for_age_greater_than_18():
    assert is_adult(19)

def test_is_adult_for_age_greater_than_18():
     assert is_adult(18)

def test_is_adult_for_age_less_than_18():
    assert not is_adult(17)
